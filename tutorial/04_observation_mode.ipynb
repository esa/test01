{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(observation_mode)=\n",
    "# Observation mode\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "\n",
    "Matej Arko\n",
    "\n",
    "## Keywords\n",
    "\n",
    "Observation mode\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`first_simulation`  | Necessary | Background |\n",
    "| {ref}`configuration` | Helpful | | \n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "\n",
    "## Learning Goals\n",
    "\n",
    "In this notebook we will run observation mode. In observation mode, the pipeline is run multiple times with different parameters, specified by the user in the configuration file. We use the library `Xarray`  to save output data for each parameter in a dataset. User can change model parameters as well as detector parameters. Parameters can be specified in the configuration file or loaded from file.\n",
    "\n",
    "By the end of the lesson you will know how to:\n",
    "* Prepare the configuration file for observation mode\n",
    "* Run observation mode\n",
    "* Inspect and save the observation results\n",
    "\n",
    "Read more:\n",
    "[Xarray](http://xarray.pydata.org/en/stable/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Different modes of parametric pipeline\n",
    "\n",
    "### Product\n",
    "  (Default mode of parametric)\n",
    "\n",
    "  ```yaml\n",
    "  parameters:\n",
    "    - key: pipeline.photon_generation.model1.arguments.argument1\n",
    "      values: [1,2,3]\n",
    "    - key: pipeline.charge_collection.model2.arguments.argument2\n",
    "      values: ['a', 'b', 'c']\n",
    "  ```\n",
    "  \n",
    "  Pipeline runs 9 times, changing:\\\n",
    "  `(argument1 = 1, argument2 = 'a'), (argument1 = 1, argument2 = 'b'), (argument1 = 1, argument2 = 'c'),`\\\n",
    "  `(argument1 = 2, argument2 = 'a'), (argument1 = 2, argument2 = 'b'), (argument1 = 2, argument2 = 'c'),`\\\n",
    "  `(argument1 = 3, argument2 = 'a'), (argument1 = 3, argument2 = 'b'), (argument1 = 3, argument2 = 'c').`\n",
    "  \n",
    "### Sequential\n",
    "\n",
    "  ```yaml\n",
    "  parameters:\n",
    "    - key: pipeline.photon_generation.model1.arguments.argument1\n",
    "      values: [1,2,3]\n",
    "    - key: pipeline.charge_collection.model2.arguments.argument2\n",
    "      values: ['a', 'b', 'c']\n",
    "  ```\n",
    "  \n",
    "  Pipeline runs 6 times:\\\n",
    "  `(argument1 = 1), (argument1 = 2), (argument1 = 3)`, while argument2 stays default,\\\n",
    "  `(argument2 = 'a'), (argument2 = 'b'), (argument2 = 'c')`, while argument1 stays default.\n",
    "\n",
    "### Custom (uploading parameters from file)\n",
    "\n",
    "  If we have parameter sets stored in file `parameters.txt` in rows like so:\\\n",
    "  `1   a`\\\n",
    "  `2   b`\\\n",
    "  `3   c`\n",
    "  \n",
    "  and configuration file looks like this:\n",
    "\n",
    "  ```yaml\n",
    "  from_file: 'parameters.txt'\n",
    "  parameters:\n",
    "    - key: pipeline.photon_generation.model1.arguments.argument1\n",
    "      values: _\n",
    "    - key: pipeline.charge_collection.model2.arguments.argument2\n",
    "      values: _\n",
    "  ```\n",
    "\n",
    "  then pipeline runs three times:\\\n",
    "  `(argument1 = 1, argument2 = 'a'), (argument1 = 2, argument2 = 'b'), (argument1 = 3, argument2 = 'c')`\n",
    "  \n",
    "## The imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Product mode example\n",
    "\n",
    "In product mode, the default parametric mode, the pipeline is run for each combination of the input parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating running mode, detector and pipeline objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"observation_product.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result1 = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Examples of plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out_1a = hv.Dataset(result1[\"image\"])\n",
    "plot_1a = out_1a.to(hv.Image, [\"x\", \"y\"], dynamic=True)\n",
    "plot_1a.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out_1b = hv.Dataset(result1[\"image\"].isel(x=50))\n",
    "plot_1b = out_1b.to(hv.Curve, [\"y\"], dynamic=True)\n",
    "plot_1b.opts(opts.Curve(aspect=1.5))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_1a + plot_1b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Custom mode example\n",
    "\n",
    "In custom mode, the parameters are loaded from file and pipeline runs as many times as there are row in the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation_custom.yaml\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "observation = config.observation\n",
    "detector = config.ccd_detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result2 = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Examples of plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out_2 = hv.Dataset(result2[\"image\"])\n",
    "plot_2 = out_2.to(hv.Image, [\"x\", \"y\"], dynamic=False)\n",
    "plot_2.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sequential mode example\n",
    "\n",
    "In sequential mode, the pipeline is run as many times as there are different parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"observation_sequential.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.ccd_detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result3 = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Examples of plots (PTC curve)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "out_3 = hv.Dataset(result3[\"image\"].std([\"x\", \"y\"]))\n",
    "plot_3 = out_3.to(hv.Scatter, [\"id\"])\n",
    "plot_3.opts(logx=True, logy=True, aspect=1.5, ylabel=\"Noise\")"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
