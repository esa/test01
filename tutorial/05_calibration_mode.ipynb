{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(calibration_mode)=\n",
    "# Calibration mode\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "\n",
    "Matej Arko\n",
    "\n",
    "## Keywords\n",
    "\n",
    "Calibration mode, genetic algorithm, Pygmo\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`first_simulation`  | Necessary | Background |\n",
    "| {ref}`configuration` | Helpful | | \n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [Dask/Distributed](https://distributed.dask.org/en/stable/quickstart.html) | Helpful | |\n",
    "\n",
    "## Learning Goals\n",
    "\n",
    "By the end of the lesson you will know how to:\n",
    "* Prepare the configuration file for calibration mode\n",
    "* Run calibration mode\n",
    "* Inspect and save the calibration results\n",
    "\n",
    "## Summary\n",
    "\n",
    "In this notebook we will try out the calibration mode. The basic idea of calibration mode is to find a set of optimal model parameters, that will when applied to the **input** data set, give a result that is as close as possible to the **target** data set. To make fitting as efficient as possible, we make use of evolutionary algorithms, implemented in library `PyGMO`. Because of a heavy computation load, we also use `Dask` for parallelization, using multiple processor threads.\n",
    "\n",
    "Calibration of a model in PyGMO nomenclature goes as follows: each processor thread is called an `island`. On each island lives a population of `individuals` (solution candidates), each randomly assigned with different `chromosomes` (model parameters). The individual with the best `fitness` (best solution) is called the `champion`. A group of islands is called an archipelago. During the evolution population is evolved multiple times with a chosen `algorithm`, each `evolution` consisting of multiple `generations`. The evolution is based on the principle of chromosome mutations. Populations can also `migrate` between the islands, taking with them the information about the best individuals. The result is that the individuals in the population get fitter and fitter. Finally, the best champion from each of the islands is extracted which gives the best model parameters.\n",
    "\n",
    "We use library `Xarray` to store the results as a data set.\n",
    "\n",
    "\n",
    "Read more:\n",
    "[PyGMO](https://esa.github.io/pygmo2/) | [Dask](https://docs.dask.org/en/latest) | [Xarray](http://xarray.pydata.org/en/stable/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set a scheduler for ``Dask``\n",
    "\n",
    "From `dask.distributed` documentation: the Client is the primary entry point for users of `dask.distributed`. The Client registers itself as the default Dask scheduler, and so runs all dask collections like `dask.array`, `dask.bag`, `dask.dataframe` and `dask.delayed`. `Dask.distributed` is a lightweight library for distributed computing in Python. It extends both the concurrent.futures and dask APIs to moderate sized clusters. It is a centrally managed, distributed, dynamic task scheduler. The central dask-scheduler process coordinates the actions of several dask-worker processes spread across multiple machines. See more on Dask distributed at https://distributed.dask.org/en/latest/.\n",
    "\n",
    "By starting the Client we get a link to the Dask dashboard when we can track the progress of the calibration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a 'local' Cluster\n",
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create running mode, detector and pipeline objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"calibration.yaml\")\n",
    "\n",
    "calibration = config.calibration  # class Calibration\n",
    "detector = config.detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot the inputs\n",
    "\n",
    "We can plot the input and target data with function `display_calibration_inputs` from `pyxel.notebook`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "pyxel.display_calibration_inputs(calibration, detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run calibration\n",
    "\n",
    "When we run the calibration mode, the islands are constructed and populated first. After the evolution starts and we can track the progress of calibration in dask dashboard. Output of the `calibration_mode` function is a named tuple of datasets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "data_tree = pyxel.run_mode(\n",
    "    mode=calibration,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The results\n",
    "\n",
    "The result of the calibration mode is a named tuple containing `dataset`, `logs`, `filenames` and `processors`. `Dataset` contains the simulated detector arrays computed for the best set of parameters - best champion from each of the islands and each of the input data sets (noted by parameter_id). It also contains the target data, which can be used for comparison. Beside this it also holds information about the champion fitness, their decision vectors and parameters for each evolution and island. Because champion fitness in the result is not saved for each generation, we can also use `logs` to inspect how the fitness evolved over time. In `filenames` we save the information about the output filenames for saved data to disk and `processors` give us the ability to retrieve and compute the simulated data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
